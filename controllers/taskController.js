const Task = require("../models/task");

// Controller function for getting all the tasks
// Defines the functions to be used in the "taskRoute.js" file and exports these functions
// module.exports, this tells us to export getAllTasks function
module.exports.getAllTasks = () => {
	
	// The "return" statement, returns the result of the Mongoose method "find" back to the "taskRoute.js" file which invokes this function when the "/tasks" routes is accessed
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then((result) => {
		// The "return" statement returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	})
}

// The request body coming from the client was passed from the "taskRoute.js" file via "req.body" as an arguments and is renamed a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	// Creates a task object based on the Mongoose model "task"
	let newTask = new Task({

		// Sets the "name" property with the value received from the client/postman
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if (error) {
			console.log(error);
			// The else statement will no longer be evaluated
			
			return false;
		// Save successful, returns the new task object back to the client/Postman
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {

	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(err){
			console.log(err);
			return false;

		// Delete successful, returns the removed task object back to the client/Postman
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {

	// The "findById" Mongoose method will look for a task with the same id provided from the URL
	// "findById" is the same as "find({"_id" : value})"
	// The "return" statement, returns the result of the Mongoose method "findById" back to the "taskRoute.js" file which invokes this function 
	return Task.findById(taskId).then((result, error) => {
		
		// If an error is encountered returns a "false" boolean back to the client/Postman
		if(error){
			console.log(error);
			return false;
		}

		// Results of the "findById" method will be stored in the result parameter
		result.name = newContent.name;

		// Saves the updated object in the MongoDB database
		return result.save().then((updateTask, saveErr) => {
			
			// If an error is encountered returns a "false" boolean back to the client/Postman
			if (saveErr){
				
				// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				console.log(saveErr);
				return false;
			
			// Update successful, returns the updated task object back to the client/Postman
			} else {
				
				/// The "return" statement returns a "false" boolean to the "then" method chained to the "save" method which invokes this function
				return updateTask;
			}
		}) 

	})
}

// Get
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((getTask) => {
		return getTask;
	})
}

// Put
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		result.status = newContent.status;

		// Saves the updated object in the MongoDB database
		return result.save().then((updateTask, saveErr) => {
			
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		}) 

	})
}